var gulp = require('gulp'),
    minifycss = require('gulp-minify-css'),
    minifyjs = require('gulp-uglify'),
    minifyhtml = require('gulp-minify-html'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-sass'),
    cssimport = require('gulp-cssimport');

var paths = {
    scss: 'app/scss',
    bowerDir: 'app/bower_components',
    js: 'app/js/**/*.js',
    templates: 'app/templates/**/*.html',
    images: 'app/images/**/*',
    jsonDataFile: 'app/json/'
};

var dest = {
    css: 'build/css/',
    js: 'build/js/',
    templates: 'build/templates/',
    images: 'build/images/',
    jsonDataFile: 'build/json/'
};

var bowerJs = [
    'app/bower_components/jquery/dist/jquery.js',
    'app/bower_components/json2-js/json2.js',
    'app/bower_components/underscore/underscore.js',
    'app/bower_components/backbone/backbone.js',
    'app/bower_components/backbone.babysitter/lib/backbone.babysitter.js',
    'app/bower_components/backbone.wreqr/lib/backbone.wreqr.js',
    'app/bower_components/marionette/lib/backbone.marionette.js'
];

gulp.task('css', function () {
    return gulp.src([paths.scss + '/style.scss'])
        .pipe(sass({
            style: 'expanded',
            compass: true,
            sourcemap: true
        }))
        .pipe(concat('style.css'))
        .pipe(cssimport())
        .pipe(autoprefixer('last 2 version'))
        .pipe(minifycss())
        .pipe(gulp.dest(dest.css));
});

gulp.task('jslibs', function () {
    return gulp.src(bowerJs)
        .pipe(concat('libs.js'))
        .pipe(minifyjs())
        .pipe(gulp.dest(dest.js));
});

gulp.task('jsapp', function () {
    return gulp.src(paths.js)
        .pipe(sourcemaps.init())
        .pipe(concat('app.js'))
        .pipe(minifyjs())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(dest.js));
});

gulp.task('js', ['jslibs', 'jsapp']);

gulp.task('templates', function () {
    return gulp.src([paths.templates])
        .pipe(gulp.dest(dest.templates));
});

gulp.task('images', function () {
    return gulp.src(paths.images)
        .pipe(gulp.dest(dest.images));
});

gulp.task('watch', function () {
    gulp.watch(paths.scss + "/**/*.scss", ['css']);
    gulp.watch(paths.js, ['jsapp']);
    gulp.watch(paths.templates, ['templates']);
});

gulp.task('build', ['css', 'js', 'templates', 'images']);
gulp.task('default', ['build']);

describe ('MyApp.ThumbModel', function() {
    it ('should be defined', function() {
        expect(MyApp.ThumbModel).toBeDefined();
    });

    it ('can be instantiated', function() {
        var task = new MyApp.ThumbModel();
        expect(task).not.toBeNull();
    });
});

describe('MyApp.ThumbModel', function() {
    beforeEach(function() {
        this.task = new MyApp.ThumbModel();
    });

    describe('new instance default values', function() {
        it('has default value for the imageUrl attribute', function() {
            expect(this.task.get('imageUrl')).toEqual('build/images/default.jpg');
        });

        it('has default value for the title attribute', function() {
            expect(this.task.get('title')).toEqual('Default thumb');
        });
    });
});

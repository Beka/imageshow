describe ('MyApp.ThumbCollection', function() {
    it ('should be defined', function() {
        expect(MyApp.ThumbsCollection).toBeDefined();
    });

    it ('can be instantiated', function() {
        var task = new MyApp.ThumbsCollection();
        expect(task).not.toBeNull();
    });
});

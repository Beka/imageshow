var MyApp = new Marionette.Application();

// Define the basic model for our data
MyApp.ThumbModel = Backbone.Model.extend({
    defaults: {
        imageUrl: 'build/images/default.jpg',
        title: 'Default thumb'
    }
});

// Define a collection filled with ThumbModel models
MyApp.ThumbsCollection = Backbone.Collection.extend({
    model: MyApp.ThumbModel,
    url: "app/json/data.json"
});

// Define a thumb view
MyApp.ThumbView = Marionette.ItemView.extend({
    template: "#template",
    tagName: "li"
});

// Define a collection view for displaying all the thumb views
MyApp.ThumbsCollectionView = Marionette.CollectionView.extend({
    tagName: "ul",
    childView: MyApp.ThumbView
});

// A working region on the page
MyApp.addRegions({
    mainRegion: "#main-region"
});

// On application start, fetch the data from json and load it into ThumbsCollection
// At the end attach the new generated view to the region
MyApp.on("start", function () {
    var collection = new MyApp.ThumbsCollection();
    collection.fetch({
        success: function (collection, response, options) {
            var myview = new MyApp.ThumbsCollectionView({
                collection: collection
            });

            MyApp.mainRegion.show(myview);
        },
        error: function (collection, response, options) {
            console.log("error");
            console.log(response.responseText);
        }
    });
});

MyApp.start();
